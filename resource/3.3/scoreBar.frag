#version 330 core

in vec2 fragCoord;
in vec3 fragColor;
out vec4 color;

uniform sampler2D image;

uniform float alpha;

void main()
{
    color = texture(image, fragCoord);
    color.rgb = 1 - color.rgb * (1 - fragColor);
    color.a = color.a * alpha;
}