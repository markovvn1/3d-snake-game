#version 300 es

precision mediump float;

in vec2 fragCoord;
out vec4 color;

uniform sampler2D image;

uniform bool horizontal;

uniform int count;
uniform float weight[48];

void main()
{
    if (count <= 1)
    {
        color = vec4(texture(image, fragCoord).rgb, 1);
        return;
    }

    vec2 imageSize = vec2(textureSize(image, 0));

    vec3 result = texture(image, fragCoord).rgb * weight[0];

    if (horizontal)
    {
        for (int i = 1; i < count; i++)
        {
            result += texture(image, fragCoord + vec2(float(i) / imageSize.x, 0)).rgb * weight[i];
            result += texture(image, fragCoord - vec2(float(i) / imageSize.x, 0)).rgb * weight[i];
        }
    }
    else
    {
        for (int i = 1; i < count; i++)
        {
            result += texture(image, fragCoord + vec2(0, float(i) / imageSize.y)).rgb * weight[i];
            result += texture(image, fragCoord - vec2(0, float(i) / imageSize.y)).rgb * weight[i];
        }
    }

    color = vec4(result, 1);
}