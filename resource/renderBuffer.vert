#version 300 es

layout (location = 0) in vec2 position; // Устанавливаем позицию атрибута в 0

// Фактический размер
uniform vec2 uniformSize;

out vec2 fragCoord;

void main()
{
    gl_Position = vec4(position.x * 2.0 - 1.0, position.y * 2.0 - 1.0, 0.0, 1.0);
    fragCoord = position * uniformSize;
}