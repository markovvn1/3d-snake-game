#version 300 es

precision mediump float;

in vec2 fragCoord;
in vec3 fragColor;
out vec4 color;

uniform sampler2D image;

uniform float alpha;

void main()
{
    color = texture(image, fragCoord);
    color.rgb = 1.0 - color.rgb * (1.0 - fragColor);
    color.a = color.a * alpha;
}