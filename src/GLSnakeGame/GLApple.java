package GLSnakeGame;

import static org.lwjgl.opengl.GL33.*;
import static GLSnakeGame.GLU.*;

public class GLApple
{
    private static boolean initialize = false;
    private static final long ANIMATE_ROTATION_PERIOD = (long) 1.3e10;
    private static final long ANIMATE_CHANGE_MODE_PERIOD = (long) 0.3e9;
    private static int id;

    private float x = 0.5f, y = 0.5f;
    private boolean mode = false;
    private long modeStartTime; // Время последней смены режима (используется для анимации появления/исчезания)

    GLApple()
    {
        assert initialize : "call .init() before start";

        hide(false);
    }

    // Генерирует яблоко с разрешением verticalCount по высоте и cycleCount по кругу
    public static void init(int verticalCount, int cycleCount)
    {
        if (initialize) return;
        initialize = true;

        // Предпросчет
        float[] pre = new float[verticalCount * 2 * 2];

        for (int i = 0; i < verticalCount * 2; i++)
        {
            float a = (float)i / (verticalCount * 2 - 1);
            a = (2 * a - 1) * 2.4188584057763776f;

            pre[i * 2 + 0] = (float)((Math.cos(a) * 0.2 + 0.15) * (Math.sin(a) + 7) / 7);
            pre[i * 2 + 1] = (float)(Math.sin(a) * 0.3);
        }

        id = glGenLists(1);
        glNewList(id, GL_COMPILE);
        glScalef(0.8f, 0.8f, 0.8f);

        glBegin(GL_TRIANGLES);

        glColor3f(242 / 255f, 190 / 255f, 17 / 255f);

        float[] evenPoints = new float[verticalCount * 3];
        float[] oddPoints = new float[verticalCount * 3];

        for (int i = 0; i <= cycleCount; i++)
        {
            float a = (float)((i < cycleCount ? i : 0) / (float) cycleCount * Math.PI * 2);
            float sin = (float) Math.sin(a);
            float cos = (float) Math.cos(a);

            float[] currentPoints = i % 2 == 0 ? evenPoints : oddPoints;

            for (int j = 0; j < verticalCount; j++)
            {
                int preId = (j * 2 + (i % 2)) * 2;

                currentPoints[j * 3 + 0] = pre[preId + 0] * cos;
                currentPoints[j * 3 + 1] = pre[preId + 1];
                currentPoints[j * 3 + 2] = pre[preId + 0] * sin;
            }

            if (i == 0) continue;

            for (int j1 = 0; j1 < verticalCount - 1; j1++)
            {
                int j2 = j1 + 1;

                // Построение треугольника even[j1], even[j2], odd[j1]
                gluMyNormal(evenPoints, evenPoints, oddPoints, j1 * 3, j2 * 3, j1 * 3, i % 2 == 0);
                glVertex3f(evenPoints[j1 * 3 + 0], evenPoints[j1 * 3 + 1], evenPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);

                // Построение треугольника odd[j1], odd[j2], even[j2]
                gluMyNormal(oddPoints, oddPoints, evenPoints, j2 * 3, j1 * 3, j2 * 3, i % 2 == 0);
                glVertex3f(oddPoints[j2 * 3 + 0], oddPoints[j2 * 3 + 1], oddPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
            }
        }

        glEnd();


        // Веточка
        cycleCount /= 2;

        pre = new float[cycleCount * 2 * 2]; // Круг
        float[] buffer = new float[verticalCount * 2]; // Скелет ветки

        for (int i = 0; i < cycleCount * 2; i++)
        {
            float a = (float) ((i / (float)(cycleCount * 2)) * Math.PI * 2);

            pre[2 * i + 0] = (float) Math.cos(a);
            pre[2 * i + 1] = (float) Math.sin(a);
        }

        for (int i = 0; i < verticalCount; i++)
        {
            float a = (verticalCount - 1 - i) / (float) (verticalCount - 1) * 0.35f + 0.15f;

            buffer[i * 2 + 0] = (5f - (float) Math.sqrt(40.8745078664f - 80 * a)) / 40;
            buffer[i * 2 + 1] = a;
        }



        evenPoints = new float[cycleCount * 3];
        oddPoints = new float[cycleCount * 3];

        glColor3f(94 / 255f, 50 / 255f, 7 / 255f);

        for (int i = 0; i < verticalCount - 1; i++)
        {
            float r = 0.01f + 0.01f / ((i * 20) / (float)(verticalCount) + 1);

            float dx = buffer[2 * (i + 1) + 1] - buffer[2 * i + 1];
            float dy = buffer[2 * i + 0] - buffer[2 * (i + 1) + 0];
            assert dx * dx + dy * dy > 1e-9f;
            float kd = ((float) Math.sqrt(dx * dx + dy * dy)) / r;

            float[] currentPoints = i % 2 == 0 ? evenPoints : oddPoints;

            for (int j = 0; j < cycleCount; j++)
            {
                int preId = (j * 2 + (i % 2)) * 2;

                float k = pre[preId + 0] / kd;

                currentPoints[j * 3 + 0] = dx * k + buffer[2 * i + 0];
                currentPoints[j * 3 + 1] = dy * k + buffer[2 * i + 1];
                currentPoints[j * 3 + 2] = pre[preId + 1] * r;
            }

            if (i == 0)
            {
                glBegin(GL_POLYGON);

                glNormal3f(buffer[0 + 0] - buffer[2 + 0], buffer[0 + 1] - buffer[2 + 1], 0);

                for (int j = 0; j < cycleCount; j++)
                    glVertex3f(currentPoints[j * 3 + 0], currentPoints[j * 3 + 1], currentPoints[j * 3 + 2]);

                glEnd();

                continue;
            }

            // Строим оболочку для evenPoints и oddPoints
            for (int j1 = 0; j1 < cycleCount; j1++)
            {
                int j2 = j1 + 1;
                if (j2 >= cycleCount) j2 = 0;

                glBegin(GL_TRIANGLES);

                // Построение треугольника even[j1], even[j2], odd[j1]
                gluMyNormal(evenPoints, evenPoints, oddPoints, j1 * 3, j2 * 3, j1 * 3, i % 2 == 1);
                glVertex3f(evenPoints[j1 * 3 + 0], evenPoints[j1 * 3 + 1], evenPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);

                // Построение треугольника odd[j1], odd[j2], even[j2]
                gluMyNormal(oddPoints, oddPoints, evenPoints, j2 * 3, j1 * 3, j2 * 3, i % 2 == 1);
                glVertex3f(oddPoints[j2 * 3 + 0], oddPoints[j2 * 3 + 1], oddPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);

                glEnd();
            }
        }


        glEndList();
    }

    public static void deinit()
    {
        glDeleteLists(id, 1);
    }

    public void build()
    {
        long modeDeltaTime = System.nanoTime() - modeStartTime;
        if (modeDeltaTime > ANIMATE_CHANGE_MODE_PERIOD && mode == false) return;

        float scale = 1;

        // Если сейчас идет анимация
        if (modeDeltaTime < ANIMATE_CHANGE_MODE_PERIOD)
        {
            scale = 1 - ((modeDeltaTime % ANIMATE_CHANGE_MODE_PERIOD) / (float) ANIMATE_CHANGE_MODE_PERIOD);
            scale *= scale;

            if (mode == true)
                scale = 1 - scale;
        }

        float a1 = ((System.nanoTime() % ANIMATE_ROTATION_PERIOD) / (float) ANIMATE_ROTATION_PERIOD) * 360;
        float a2 = a1 / 180 * (float)Math.PI;

        glPushMatrix();
        glTranslatef(x, 0.25f + (float)Math.sin(a2 * 5) * 0.08f, y);
        glScalef(scale, scale, scale);
        glRotatef(a1, 0, 1, 0);

        glCallList(id);

        glPopMatrix();
    }

    public void setPos(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public void show()
    {
        show(true);
    }

    public void show(boolean animation)
    {
        if (mode == true) return;

        mode = true;
        modeStartTime = System.nanoTime();
        if (!animation) modeStartTime -= ANIMATE_CHANGE_MODE_PERIOD;
    }

    public void hide()
    {
        hide(true);
    }

    public void hide(boolean animation)
    {
        if (mode == false) return;

        mode = false;
        modeStartTime = System.nanoTime();
        if (!animation) modeStartTime -= ANIMATE_CHANGE_MODE_PERIOD;
    }


}
