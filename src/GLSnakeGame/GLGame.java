package GLSnakeGame;

import static org.lwjgl.opengl.GL33.*;
import static GLSnakeGame.GLU.*;

/*
    Так как с этим классом предполагается работать пользователю, когда он захочет вставить этот движок в свой код, тут
    очень много assert'ов - чтобы упросить процесс дебага.

    Важная особенность движка:
    Так как движок трехмерный, при построении маршрута змейки ему необходимо знать куда змейка будет двигаться
    в будущем - чтобы выстроить дугу в правильном направлении. И, так как в будущее мы пока смотреть не умеем,
    движок рисует с опозданием на один блок. А значит при вызове метода .updateSnake() нужно передавать в качестве
    параметров текущие координаты, но при этом движок будет считать их как следующие - координаты, где змейка
    окажется при следующем вызове этой функции.
    Движок сам задерживает все события (рост змейки, появления яблока и так далее), поэтому в него нужно отправлять
    события так, как будто ничего не задерживается. Единственное отличие это конец. После смерти змеек нужно отправить движку место, где они были бы, если бы не умерли. Движку все равно
    на отрицательные числа или пересечение змеек


    Как работать:
    1. Вызвать .init() для инициализации системы
    2. Передать информацию о начальном положении змейки с помощью .updateSnake()
    3. Вызвать .start() для перевода движка в режим рендера и открытия окна
    4. В цикле:
        4.1. Вызвать .nextFrame() чтобы сообщить движку что начался новый кадр (кадром тут называется состояние, когда
        голова змейки находится на определенной клетке; если голова двигается в этой клетке, это дробная часть кадра)
        4.2. Отправить движку новую голову змейки вызовом метода .updateSnake()
        4.3. При необходимости установить новое положение яблока вызовом .appleEatenBy() .setApplePos()
        4.4. Вывести результат на экран вызовом .render()
 */

class GLSnakeData
{
    public boolean initialize = false;

    public int lastX, lastY; // Хранит две последнюю точку
    public int lastCommand = -1; // Хранит последнюю команду
    public int lastLength; // Хранит последнюю длину змейки

    // Хранит то, до какого кадра нужно анимировать змейку (используется при окончании игры)
    public int maxFrame = -1;
    public float maxA;
}

public class GLGame
{
    private GLRenderBuffer renderBuffer = new GLRenderBuffer();
    private GLWindow window = new GLWindow();
    private int lastWidth = -1, lastHeight = -1;

    private GLScoreBar scoreBar = new GLScoreBar();
    private int scoreA = 0, scoreB = 0;

    private GLSnake[] glSnakes;
    private GLSnakeData[] glSnakesData;

    private GLMaze glMaze = new GLMaze();

    private GLApple glApple, glLastApple;
    private int appleEventFrame = -1; // Кадр, на котором должно произойти событие с яблоком
    private int appleEatenBy = -1; // Index змейки, которая слопала яблоко
    private float appleX, appleY; // Координаты нового яблока

    private int frame = -1;
    private int maxFrame = -1;
    private float maxA = 0f;

    // состояние движка:
    //     -2 - неактивен
    //     -1 - режим инициализации
    //      0 - режим работы
    //      1 - режим завершения работы
    private int mode = -2;


    // Инициализация графического интерфейса
    public void init(int mazeSizeWidth, int mazeSizeHeight, int amountOfSnakes, boolean logs)
    {
        assert mode == -2 : "GLGame has initialized";
        mode = -1;

        assert mazeSizeWidth > 2 && mazeSizeHeight > 2 : "Maze too small";
        assert mazeSizeHeight * mazeSizeWidth < 256 : "Maze too big";
        assert amountOfSnakes >= 0 && amountOfSnakes <= 2 : "amountOfSnakes should be in range [0; 2]";

        if (logs) System.out.println("[Logs]: Creating window");
        window.init(900, 700, "Snake game | by Markovvn1");

        // init OpenGL
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_LIGHTING);
        glEnable(GL_NORMALIZE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        if (logs) System.out.println("[Logs]: Creating buffers for render, initialize shaders");
        renderBuffer.init();
        if (logs) System.out.println("[Logs]: Load texture for scoreBar");
        scoreBar.init();
        if (logs) System.out.println("[Logs]: Preparing snakes");
        GLSnake.init(0.18f, 8);
        glSnakes = new GLSnake[amountOfSnakes];
        glSnakesData = new GLSnakeData[amountOfSnakes];


        for (int i = 0; i < amountOfSnakes; i++)
        {
            glSnakes[i] = new GLSnake();
            glSnakes[i].setPointPerCell(10f);

            glSnakesData[i] = new GLSnakeData();
        }

        glSnakes[0].setColor(217 / 255f, 47 / 255f, 28 / 255f);
        scoreBar.setColorA(191 / 255f, 23 / 255f, 11 / 255f);
        glSnakes[1].setColor(70 / 255f, 47 / 255f, 222 / 255f);
        scoreBar.setColorB(11 / 255f, 28 / 255f, 117 / 255f);


        if (logs) System.out.println("[Logs]: Creating maze");
        glMaze.init(mazeSizeWidth, mazeSizeHeight);

        if (logs) System.out.println("[Logs]: Precomputation for apple");
        GLApple.init(10, 16);
        glApple = new GLApple();
        glLastApple = new GLApple();



        float light0_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
        float light0_ambient[] = {0.1f, 0.1f, 0.1f, 1.0f};

        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
        glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
    }

    public void deinit()
    {
        if (mode == -2) return;
        mode = -2;

        glMaze.deinit();
        GLApple.deinit();
        renderBuffer.deinit();
        scoreBar.deinit();
        window.deinit();
    }

    public void start()
    {
        assert mode == -1 : "You must call .init() before this";

        for (int i = 0; i < glSnakes.length; i++)
            assert glSnakesData[i].lastX >= 0 : "You have to call .initSnake(" + i + ", ...) and set initial position before this";

        mode = 0;
        frame = -1;
        maxFrame = -1;

        window.show();
    }

    // Game over
    public void end()
    {
        assert mode == 0 : "Game do not start";

        mode = 1;
    }

    private void initSnake(int index, int x, int y)
    {
        GLSnakeData snakeData = glSnakesData[index];

        assert mode < 0 : "You have to call this method before .start()";
        assert mode == -1 : "You have to call .init() before this";
        assert 0 <= index && index <= glSnakes.length : "Incorrect index";
        assert x >= 0 && y >= 0 && x < glMaze.getWidth() && y < glMaze.getHeight() : "Incorrect coordinates";

        snakeData.initialize = true;
        snakeData.lastX = x;
        snakeData.lastY = y;
        snakeData.lastLength = 1;
    }

    public void updateSnake(int index, int x, int y, int length)
    {
        updateSnake(index, x, y, length, GLTypeOfCollision.NO_COLLISION);
    }

    // Построить дальнейший путь змейки
    // length - длина змейки после выполнения этого шага
    public void updateSnake(int index, int x, int y, int length, GLTypeOfCollision type)
    {
        if (mode < 1)
            assert type == GLTypeOfCollision.NO_COLLISION : "You can set collision type only at the end of game (call .end() before)";

        GLSnakeData snakeData = glSnakesData[index];

        // Если для этой змейки эта функция еще не вызывалась
        if (!snakeData.initialize)
        {
            assert length == 1 : "Incorrect length. For first call length have to be 1";

            initSnake(index, x, y);

            return;
        }

        assert 0 <= index && index <= glSnakes.length : "Incorrect index";
        assert length >= 0 && length <= snakeData.lastLength + 1 : "Incorrect length: " + snakeData.lastLength + " -> " + length;

        int dx = x - snakeData.lastX;
        int dy = y - snakeData.lastY;

        assert Math.abs(dx) + Math.abs(dy) == 1 :
                "Snake have moved incorrect: (" + snakeData.lastX + ", " + snakeData.lastY + ") -> (" + x + ", " + y + ")";

        int command = 0;
        if (dy == 1) command = 1;
        if (dx == -1) command = 2;
        if (dy == -1) command = 3;

        // Если это первый вызов этой функции
        if (snakeData.lastCommand < 0)
        {
            // то будем считать что до этого змейка двигалась так же, как и сейчас
            snakeData.lastCommand = command;
        }

        glSnakes[index].move(snakeData.lastX, snakeData.lastY,
                             snakeData.lastCommand, command, snakeData.lastLength);

        snakeData.lastX = x;
        snakeData.lastY = y;
        snakeData.lastCommand = command;
        snakeData.lastLength = length;

        if (mode == 1) endSnake(index, type);
    }

    // You should call this after game is over and after calling .end()
    // You have to put here the position where snake's head will be if there are not collision
    private void endSnake(int index, GLTypeOfCollision type)
    {
        GLSnakeData snakeData = glSnakesData[index];
        snakeData.maxFrame = frame;
        snakeData.maxA = 1f;

        if (type == GLTypeOfCollision.NO_COLLISION) return;

        switch (type)
        {
            case TO_WALL:
                snakeData.maxFrame = frame;
                snakeData.maxA = 0.4f;
                break;
            case TO_BODY:
                snakeData.maxFrame = frame;
                snakeData.maxA = 0.9f;
                break;
            case TO_HEAD:
                snakeData.maxFrame = frame;
                snakeData.maxA = 0.95f;
                break;
        }

        if (snakeData.maxFrame > maxFrame)
        {
            maxFrame = snakeData.maxFrame;
            maxA = snakeData.maxA;
        }
        else if (snakeData.maxFrame == maxFrame && snakeData.maxA > maxA) maxA = snakeData.maxA;
    }

    // Установка положения камеры, освещения и так далее
    private void initRender(float a, boolean updateProjection)
    {
        // Only when resize
        if (updateProjection)
        {
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();

            gluPerspective(60, (float) lastWidth / lastHeight, 0.01f, 40);
        }


        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        gluLookAt(0, 7, -7,
                0, 0, 0,
                0,1, 0);



        glClearColor(184f / 255, 221f / 255, 222f / 255, 1f);
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        float light0_direction[] = {1.0f, 3.0f, -2.0f, 0.0f};
        glLightfv(GL_LIGHT0, GL_POSITION, light0_direction);

    }

    // Собственно вывод самой сцены
    private void renderScene(int frame, float a, boolean updateProjection)
    {
        glEnable(GL_DEPTH_TEST);

        // Установка положения камеры, освещения и так далее
        initRender(a, updateProjection);

        // Построение сцены в центре мира
        glMaze.build();

        // Перемещение в угол поля
        glTranslatef(-glMaze.getWidth() / 2f, 0, -glMaze.getHeight() / 2f);

        // Рендер змеек
        for (int i = 0; i < glSnakes.length; i++)
        {
            float currentA = a;
            if (mode == 1)
            {
                // Проверка что змейка не сместилась дальше чем можно конкретно для этой змейки
                if (frame > glSnakesData[i].maxFrame)
                    currentA = glSnakesData[i].maxA;

                if (frame == glSnakesData[i].maxFrame && currentA > glSnakesData[i].maxA)
                    currentA = glSnakesData[i].maxA;
            }

            glSnakes[i].build(currentA);
        }

        // Построение яблока
        glApple.build();
        glLastApple.build();
    }

    // Что-то типа менедрежа рендеринга - решает что когда и как рендерить и какие посэфекты проводить
    // Показать положение змеек, смещенное на (1 - a) клеток назад (а в промежутке от [0; 1])
    // Для плавности движения стоит потихоньку увеличивать значение параметра a
    public boolean render(int frame, float a)
    {
        assert mode >= 0 : "You have to call .start() before this";
        assert a >= 0 && a <= 1 : "a have to be in range [0; 1]";
        if (mode == 0)
        {
            assert frame >= 0 : "You have to call .nextFrame() before this";
            assert this.frame == frame : "Incorrect amount of frame (maybe you do not call .nextFrame()) or time goes too fast (" + this.frame + " -> " + frame + ")";
        }

        boolean resize = window.getWidth() != lastWidth || window.getHeight() != lastHeight;

        if (resize)
        {
            lastWidth = window.getWidth();
            lastHeight = window.getHeight();

            glViewport(0, 0, lastWidth, lastHeight);
        }

        // Имеет ли смысл рендерить новый кадр? или игра уже закончина и можно и старый показать?
        boolean renderActive = true;

        if (mode == 1)
        {
            if (frame > maxFrame) renderActive = false;
            if (frame == maxFrame && a > maxA) renderActive = false;
        }


        if (renderActive)
        {
            renderScene(frame, a, resize);
        }
        else
        {
            // Сколько времени прошло с момента окончания игры?
            float overA = frame - maxFrame + a - maxA;
            renderActive = overA < 1.5f;

            // Показывать последний кадр
            frame = maxFrame;
            a = maxA;

            // Need to rerender
            if (renderBuffer.needToRender() || resize)
            {
                renderBuffer.startRecord(lastWidth, lastHeight);
                renderScene(frame, a, resize);
                renderBuffer.stopRecord();
            }

            float smooth = (overA > 0.5f ? 1 : overA * 2);
            renderBuffer.render(smooth * smooth * 25);
            float overAshift = overA < 0.5f ? 0 : overA - 0.5f;
            scoreBar.render(lastWidth, lastHeight, scoreA, scoreB, overAshift > 0.5f ? 1 : overAshift * 2);
        }




        // Вывести картинку на экран
        glFlush(); // Почему-то эта версия OpenGL не вызывает эту команду перед сменой буфера...
        window.swapBuffers();
        window.update();

        return renderActive;
    }

    public void nextFrame()
    {
        assert mode >= 0 : "You have to call .start() before this";

        frame++;

        if (frame >= appleEventFrame && appleEventFrame != -1)
        {
            appleEventFrame = -1;

            glApple.setPos(appleX + 0.5f, appleY + 0.5f);
            glApple.show();
            glLastApple.hide();

            if (appleEatenBy != -1)
            {
                glSnakes[appleEatenBy].eatApple();
                appleEatenBy = -1;
            }
        }
    }

    public void setApplePos(int x, int y)
    {
        assert mode != -2 : "You have to call .init() before this";

        appleEventFrame = frame + 1;
        appleX = x; appleY = y;
    }

    // Когда координаты яблока поменялись
    public void appleEatenBy(int index)
    {
        assert mode >= 0 : "You have to call .start() before this";
        assert 0 <= index && index <= glSnakes.length : "Incorrect index";

        appleEatenBy = index;

        GLApple temp = glLastApple;
        glLastApple = glApple;
        glApple = temp;

        appleEventFrame = frame + 1; // Задержка один кадр
    }

    public void setScore(int a, int b)
    {
        scoreA = a;
        scoreB = b;
    }

    public GLWindow getWindow()
    {
        return window;
    }
}
