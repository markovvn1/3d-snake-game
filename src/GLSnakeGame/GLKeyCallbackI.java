package GLSnakeGame;

public interface GLKeyCallbackI
{
    // Метод будет вызываться тогда, когда произойдет событие нажатия одной из кнопки одной из группы
    // Всего существует 2 граппы кнопок: стрелки (0) и WASD (1).
    // command:
    //       1
    //      ____
    //     |    |
    //  2  |____|  0
    //
    //       3
    void keyEvent(int action, int groupNum, GLKeyCommand command);
}
