package GLSnakeGame;

import static org.lwjgl.opengl.GL33.*;

public class GLMaze
{
    private int id;
    private int width, height;

    public void init(int width, int height)
    {
        this.width = width;
        this.height = height;

        id = glGenLists(1);
        glNewList(id, GL_COMPILE);

        glPushMatrix();
        glTranslatef(-width / 2f, 0, -height / 2f);

        // Клетки
        for (int x = 0; x < width; x++)
            for (int z = 0; z < height; z++)
            {
                if ((x + z) % 2 == 0)
                    glColor3f(91f / 255, 201f / 255, 32f / 255);
                else
                    glColor3f(63f / 255, 148f / 255, 18f / 255);

                glBegin(GL_QUADS);
                glNormal3f(0, 1, 0);
                glVertex3f(0 + x, 0, 0 + z);
                glVertex3f(1 + x, 0, 0 + z);
                glVertex3f(1 + x, 0, 1 + z);
                glVertex3f(0 + x, 0, 1 + z);
                glEnd();
            }

        glPopMatrix();

        // Рамка
        glColor3f(161f / 255, 169f / 255, 179f / 255);

        for (int i = 0; i < 4; i++)
        {
            float size = (i % 2 == 0 ? width : height) / 2f;
            float shift = (i % 2 == 1 ? width : height) / 2f;
            final float BORDER_HEIGHT = 0.2f;
            final float BORDER_WIDTH = 0.2f;

            glPushMatrix();
            glTranslatef(0, 0, shift);

            glBegin(GL_QUADS);
            glNormal3f(0, 0, -1);
            glVertex3f(-size, 0, 0);
            glVertex3f(size, 0, 0);
            glVertex3f(size, BORDER_HEIGHT, 0);
            glVertex3f(-size, BORDER_HEIGHT, 0);
            glEnd();

            glBegin(GL_QUADS);
            glNormal3f(0, 1, 0);
            glVertex3f(-size, BORDER_HEIGHT, 0);
            glVertex3f(size, BORDER_HEIGHT, 0);
            glVertex3f(size + BORDER_WIDTH, BORDER_HEIGHT, BORDER_WIDTH);
            glVertex3f(-size - BORDER_WIDTH, BORDER_HEIGHT, BORDER_WIDTH);
            glEnd();

            glBegin(GL_QUADS);
            glNormal3f(0, 0, 1);
            glVertex3f(-size - BORDER_WIDTH, BORDER_HEIGHT, BORDER_WIDTH);
            glVertex3f(size + BORDER_WIDTH, BORDER_HEIGHT, BORDER_WIDTH);
            glVertex3f(size + BORDER_WIDTH, 0, BORDER_WIDTH);
            glVertex3f(-size - BORDER_WIDTH, 0, BORDER_WIDTH);
            glEnd();

            glPopMatrix();
            glRotatef(90f, 0, 1, 0);
        }

        glEndList();
    }

    public void deinit()
    {
        glDeleteLists(id, 1);
    }

    public void build()
    {
        glCallList(id);
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}
