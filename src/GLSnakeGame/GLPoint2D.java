package GLSnakeGame;

public class GLPoint2D
{
    float x, y;

    GLPoint2D(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString()
    {
        return "<" + x + ", " + y + ">";
    }
}