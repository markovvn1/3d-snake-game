package GLSnakeGame;

import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class GLRenderBuffer
{
    private int normalFBO1, normalFBO2, multisampleFBO;
    private int normalFrameBuffer1 = 0, normalFrameBuffer2 = 0;
    private int multisampleFrameBuffer = 0, multisampleDepthBuffer = 0;
    private int imageWidth = 0, imageHeight = 0;

    private float lastSmooth = -1;
    float[] weight = new float[48];

    private float uniformSizeX, uniformSizeY;

    // Shader для вывода картинки
    private GLShader shader = new GLShader();

    private int quadVAO, quadVBO;

    public void init()
    {
        float quadVertices[] = {
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 1.0f,
                1.0f, 0.0f,
        };

        quadVAO = glGenVertexArrays();
        quadVBO = glGenBuffers();
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 2 * 4, 0);


        // Create FBO
        normalFBO1 = glGenFramebuffers();
        normalFBO2 = glGenFramebuffers();
        multisampleFBO = glGenFramebuffers();


        // Load shaders
        shader.init();
        shader.loadFromResource("renderBuffer.vert", "renderBuffer.frag");
        shader.compile();
    }

    public void deinit()
    {
        glDeleteBuffers(quadVBO);
        glDeleteVertexArrays(quadVAO);

        glDeleteFramebuffers(normalFBO1);
        glDeleteFramebuffers(normalFBO2);
        glDeleteFramebuffers(multisampleFBO);

        if (normalFrameBuffer1 != 0) glDeleteTextures(normalFrameBuffer1);
        if (normalFrameBuffer2 != 0) glDeleteTextures(normalFrameBuffer2);
        if (multisampleFrameBuffer != 0) glDeleteTextures(multisampleFrameBuffer);
        if (multisampleDepthBuffer != 0) glDeleteRenderbuffers(multisampleDepthBuffer);

        shader.deinit();
    }

    public void startRecord(int width, int height)
    {
        // Нужно ли создавать новый буфер, или можно использовать старый
        boolean needToUpdate = imageWidth == 0 || imageHeight == 0;
        if (!needToUpdate) needToUpdate = width > imageWidth || height > imageHeight;
        if (!needToUpdate) needToUpdate = width * 2 < imageWidth || height * 2 < imageHeight;

        if (needToUpdate)
        {
            imageWidth = (int)(width * 1.2);
            imageHeight = (int)(height * 1.2);


            // Создание нового буфера для цвета (обычного)
            if (normalFrameBuffer1 != 0) glDeleteTextures(normalFrameBuffer1);
            normalFrameBuffer1 = glGenTextures();
            glBindTexture(GL_TEXTURE_2D, normalFrameBuffer1);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

            // Првязка его к обычному FBO
            glBindFramebuffer(GL_FRAMEBUFFER, normalFBO1);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, normalFrameBuffer1, 0);

            assert glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE : "FrameBuffer not complete";
            glClear (GL_COLOR_BUFFER_BIT);


            // Создание нового буфера для цвета (обычного) - временный буфер
            if (normalFrameBuffer2 != 0) glDeleteTextures(normalFrameBuffer2);
            normalFrameBuffer2 = glGenTextures();
            glBindTexture(GL_TEXTURE_2D, normalFrameBuffer2);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

            // Првязка его к обычному FBO
            glBindFramebuffer(GL_FRAMEBUFFER, normalFBO2);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, normalFrameBuffer2, 0);

            assert glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE : "FrameBuffer not complete";
            glClear (GL_COLOR_BUFFER_BIT);



            // Создание нового буфера для цвета (мультисемплинг)
            if (multisampleFrameBuffer != 0) glDeleteTextures(multisampleFrameBuffer);
            multisampleFrameBuffer = glGenTextures();
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, multisampleFrameBuffer);
            glTexParameterf(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA, imageWidth, imageHeight, true);

            // Создание буфера для глубины (мультисемплинг)
            if (multisampleDepthBuffer != 0) glDeleteRenderbuffers(multisampleDepthBuffer);
            multisampleDepthBuffer = glGenRenderbuffers();
            glBindRenderbuffer(GL_RENDERBUFFER, multisampleDepthBuffer);
            glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT, imageWidth, imageHeight);

            // Привязка этого всего к мультисемпл буферу
            glBindFramebuffer(GL_FRAMEBUFFER, multisampleFBO);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, multisampleFrameBuffer, 0);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, multisampleDepthBuffer);

            assert glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE : "FrameBuffer not complete";
        }

        glBindFramebuffer(GL_FRAMEBUFFER, multisampleFBO);

        uniformSizeX = width / (float) imageWidth;
        uniformSizeY = height / (float) imageHeight;
    }

    public void stopRecord()
    {
        glFlush();

        // Convert data from multisample to normal
        glBindFramebuffer(GL_READ_FRAMEBUFFER, multisampleFBO);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, normalFBO1);
        glBlitFramebuffer(0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);

        lastSmooth = -1; // Пометить что на этот буфер мы еще не применяли сглаживания

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    private void gaussianBlur(float smooth)
    {
        // Рассчет весов
        float c = smooth / 3; // rule of 3 sigm
        float k = c * 2.50662827f; // c * sqrt(2 * pi)
        c = 2 * c * c; // 2 * c ^ 2

        for (int i = 0; i < (int) smooth; i++)
            weight[i] = (float) Math.exp(-i * i / c) / k;

        assert (int)smooth <= 48 : "smooth too large";
        glUniform1fv(glGetUniformLocation(shader.getId(), "weight"), weight);
        glUniform1i(glGetUniformLocation(shader.getId(), "count"), (int)smooth);

        // Первый проход размытия по Гаусу
        glUniform1i(glGetUniformLocation(shader.getId(), "horizontal"), 1);

        glBindTexture(GL_TEXTURE_2D, normalFrameBuffer1); // input
        glBindFramebuffer(GL_FRAMEBUFFER, normalFBO2); // output

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glFlush();

        // Второй проход размытия по Гаусу
        glUniform1i(glGetUniformLocation(shader.getId(), "horizontal"), 0);

        glBindTexture(GL_TEXTURE_2D, normalFrameBuffer2); // input
        glBindFramebuffer(GL_FRAMEBUFFER, normalFBO1); // output

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glFlush();
    }

    public void render(float smooth)
    {
        glDisable(GL_DEPTH_TEST);

        glClear(GL_COLOR_BUFFER_BIT);

        shader.start();

        // Send this information to the GPU
        glUniform2f(glGetUniformLocation(shader.getId(), "uniformSize"), uniformSizeX, uniformSizeY);

        glBindVertexArray(quadVAO);

        if ((int)smooth > 1 && smooth != lastSmooth)
        {
            // Если это уже не первый вызов сглаживания
            if (lastSmooth != -1)
            {
                // Копирование сохраненной информации
                glBindFramebuffer(GL_READ_FRAMEBUFFER, multisampleFBO);
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, normalFBO1);
                glBlitFramebuffer(0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
            }

            lastSmooth = smooth;

            // Размытие
            gaussianBlur(smooth);
        }

        glUniform1i(glGetUniformLocation(shader.getId(), "count"), 0);

        glBindTexture(GL_TEXTURE_2D, normalFrameBuffer1); // input
        glBindFramebuffer(GL_FRAMEBUFFER, 0); // output

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glFlush();

        shader.stop();
    }

    // Если еще ни одного рендера не было
    public boolean needToRender()
    {
        return imageWidth == 0 || imageHeight == 0;
    }
}
