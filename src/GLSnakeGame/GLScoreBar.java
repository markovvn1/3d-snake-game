package GLSnakeGame;

import org.lwjgl.BufferUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL33.*;

public class GLScoreBar
{
    private GLShader shader;
    private int quadVAO, quadVBO;
    private float[] vertexData;

    private int textureId = 0;
    private int textureWidth, textureHeight;

    // Цвета символов
    private float[] colorA = {0.5f, 0.5f, 0.5f};
    private float[] colorB = {0.5f, 0.5f, 0.5f};
    private float[] colorBack = {0.4f, 0.4f, 0.4f};

    private boolean loadImageFromResource(String fileName)
    {
        BufferedImage tex;

        try
        {
            tex = ImageIO.read(getClass().getClassLoader().getResourceAsStream(fileName));
        }
        catch (Exception e)
        {
            return false;
        }

        textureWidth = tex.getWidth();
        textureHeight = tex.getHeight();

        assert textureWidth % 11 == 0 : "Incorrect width";


        int[] pixels = new int[textureWidth * textureHeight];
        tex.getRGB(0, 0, textureWidth, textureHeight, pixels, 0, textureWidth);

        ByteBuffer buffer = BufferUtils.createByteBuffer(4 * textureWidth * textureHeight);

        for(int y = 0; y < textureHeight; ++y) {
            for(int x = 0; x < textureWidth; ++x) {
                int pixel = pixels[y * textureWidth + x];
                // Изменение порядка каналов и инверсия картинки (понадобиться для наложения эффектов)
                buffer.put((byte) (255 - ((pixel >> 16) & 0xFF)));
                buffer.put((byte) (255 - ((pixel >> 8) & 0xFF)));
                buffer.put((byte) (255 - (pixel & 0xFF)));
                buffer.put((byte) ((pixel >> 24) & 0xFF));
            }
        }

        buffer.flip();

        textureId = glGenTextures();

        glBindTexture(GL_TEXTURE_2D, textureId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

        return true; // Return Image Address In Memory
    }

    public void init()
    {
        if (!loadImageFromResource("textureScore.png"))
            System.err.println("Cannot load textureScore.png");

        // Load shaders
        shader = new GLShader();
        shader.init();
        shader.loadFromResource("scoreBar.vert", "scoreBar.frag");
        shader.compile();

        // Создание и настройка буфера для полигонов
        quadVAO = glGenVertexArrays();
        quadVBO = glGenBuffers();
        glBindVertexArray(quadVAO);
        glEnableVertexAttribArray(0); // position
        glEnableVertexAttribArray(1); // color
        glEnableVertexAttribArray(2); // uv

        vertexData = new float[4 * 7];
    }

    public void deinit()
    {
        glDeleteTextures(textureId);

        glDeleteBuffers(quadVBO);
        glDeleteVertexArrays(quadVAO);
    }

    // Рендерить квадратик c текустурой номер tex, поместить его на позицию pos.
    private void render(int width, int height, int pos, int length, int tex, float[] color)
    {
        // Генерация данных
        vertexData[0 * 7 + 0] = textureWidth / 11 * (2 * pos - length) / (float) width;
        vertexData[1 * 7 + 0] = vertexData[0 * 7 + 0];
        vertexData[2 * 7 + 0] = textureWidth / 11 * (2 * pos - length + 2) / (float) width;
        vertexData[3 * 7 + 0] = vertexData[2 * 7 + 0];

        vertexData[0 * 7 + 1] = textureHeight / (float) height;
        vertexData[1 * 7 + 1] = -vertexData[0 * 7 + 1];
        vertexData[2 * 7 + 1] = vertexData[0 * 7 + 1];
        vertexData[3 * 7 + 1] = vertexData[1 * 7 + 1];

        for (int i = 0; i < 4; i++)
        {
            vertexData[i * 7 + 2] = color[0];
            vertexData[i * 7 + 3] = color[1];
            vertexData[i * 7 + 4] = color[2];
        }

        vertexData[0 * 7 + 5] = textureWidth / 11 * tex / (float) (textureWidth);
        vertexData[1 * 7 + 5] = vertexData[0 * 7 + 5];
        vertexData[2 * 7 + 5] = textureWidth / 11 * (tex + 1) / (float) (textureWidth);
        vertexData[3 * 7 + 5] = vertexData[2 * 7 + 5];

        vertexData[0 * 7 + 6] = 0;
        vertexData[1 * 7 + 6] = 1;
        vertexData[2 * 7 + 6] = 0;
        vertexData[3 * 7 + 6] = 1;


        // Вывод данных
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, vertexData, GL_STATIC_DRAW);

        glBindVertexArray(quadVAO);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 7 * 4, 0);
        glVertexAttribPointer(1, 3, GL_FLOAT, false, 7 * 4, 2 * 4);
        glVertexAttribPointer(2, 2, GL_FLOAT, false, 7 * 4, 5 * 4);


        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    public void render(int width, int height, int a, int b, float alpha)
    {
        assert a < 100 && b < 100 : "Too long number";
        assert a >= 0 && b >= 0 : "Incorrect number";

        int length = 1 + (a < 10 ? 1 : 2) + (b < 10 ? 1 : 2);

        glBindTexture(GL_TEXTURE_2D, textureId);

        shader.start();

        glUniform1f(glGetUniformLocation(shader.getId(), "alpha"), alpha);

        int i = 0;

        if (a >= 10)
        {
            render(width, height, i, length, a / 10 + 1, colorA);
            i++;
        }

        render(width, height, i, length, a % 10 + 1, colorA);
        i++;

        render(width, height, i, length, 0, colorBack);
        i++;

        if (b >= 10)
        {
            render(width, height, i, length, b / 10 + 1, colorB);
            i++;
        }

        render(width, height, i, length, b % 10 + 1, colorB);
        i++;


        shader.stop();
    }



    public void setColorA(float r, float g, float b)
    {
        colorA[0] = r;
        colorA[1] = g;
        colorA[2] = b;
    }

    public void setColorB(float r, float g, float b)
    {
        colorB[0] = r;
        colorB[1] = g;
        colorB[2] = b;
    }
}
