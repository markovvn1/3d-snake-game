package GLSnakeGame;

import java.io.*;
import static org.lwjgl.opengl.GL33.*;

public class GLShader
{
    int shaderProgram = 0;
    int vertexShader, fragmentShader;

    public void init()
    {
        if (shaderProgram != 0) return;

        shaderProgram = glCreateProgram();
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    }

    public void deinit()
    {
        if (shaderProgram == 0) return;

        glDeleteProgram(shaderProgram);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }

    private String loadFromResource(String fileName)
    {
        StringBuilder builder = new StringBuilder();

        BufferedReader reader =
                new BufferedReader(
                new InputStreamReader(getClass().getClassLoader().getResourceAsStream(fileName)));

        try
        {
            String line;
            while ((line = reader.readLine()) != null)
                builder.append(line).append('\n');
        }
        catch (Exception e)
        {

        }

        return builder.toString();
    }

    public boolean loadFromResource(String vertex, String fragment)
    {
        String vertexSource = loadFromResource(vertex);
        String fragmentSource = loadFromResource(fragment);

        return loadFromString(vertexSource, fragmentSource);
    }

    public boolean loadFromString(String vertexSource, String fragmentSource)
    {
        glShaderSource(vertexShader, vertexSource);
        glShaderSource(fragmentShader, fragmentSource);

        return true;
    }

    private boolean compileShader(int shader)
    {
        glCompileShader(shader);

        if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE)
        {
            System.err.println("Compile:" + glGetShaderInfoLog(shader));
            return false;
        }

        glAttachShader(shaderProgram, shader);

        return true;
    }

    public boolean compile()
    {
        if (!compileShader(vertexShader)) return false;
        if (!compileShader(fragmentShader)) return false;

        // Linking
        glLinkProgram(shaderProgram);

        if (glGetProgrami(shaderProgram, GL_LINK_STATUS) == GL_FALSE)
        {
            System.err.println("Link: " + glGetProgramInfoLog(shaderProgram));
            return false;
        }

        // Validate
        glValidateProgram(shaderProgram);

        if (glGetProgrami(shaderProgram, GL_VALIDATE_STATUS) == GL_FALSE)
        {
            System.err.println("Validate: " + glGetProgramInfoLog(shaderProgram));
            return false;
        }

        return true;
    }

    public void start()
    {
        glUseProgram(shaderProgram);
    }

    public void stop()
    {
        glUseProgram(0);
    }

    public int getId()
    {
        return shaderProgram;
    }
}
