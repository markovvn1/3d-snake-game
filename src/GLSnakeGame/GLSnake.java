package GLSnakeGame;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL33.*;
import static GLSnakeGame.GLU.*;

public class GLSnake
{
    private static final float APPLE_RADIUS = 0.2f;

    private static boolean initialize = false; // уже инициализирован?
    private static int cycleCount;
    private static float mainRadius;
    private static float[] preXY; // предпросчитанные значения для змейки
    private static FloatBuffer matrix = BufferUtils.createFloatBuffer(16);

    // память для построения каркаса
    private static float[] buffer = new float[256 * 30 + 10];
    private static float[] evenPoints, evenPointsHead;
    private static float[] oddPoints, oddPointsHead;

    private float[] color = new float[3];
    private float pointPerCell = 8;
    private GLTrajectory trajectory = new GLTrajectory();
    private int lastLength = 0, currentLength = 0;

    // eatApple
    ArrayList<Long> startEatTime = new ArrayList<Long>();



    GLSnake()
    {
        assert initialize : "call .init() before start";
    }

    // Рассчитывает все необходимые синусы и косинусы для змейки с радиусом radius и частотой дискретизации count
    public static void init(float radius, int count)
    {
        if (initialize) return;
        initialize = true;

        assert count % 2 == 0 : "count must be even number";

        mainRadius = radius;
        cycleCount = count;
        preXY = new float[cycleCount * 2 * 2];

        evenPoints = new float[cycleCount * 3];
        evenPointsHead = new float[cycleCount * 3];
        oddPoints = new float[cycleCount * 3];
        oddPointsHead = new float[cycleCount * 3];

        for (int i = 0; i < cycleCount * 2; i++)
        {
            double angle = i * Math.PI / cycleCount;

            preXY[i * 2 + 0] = (float) Math.cos(angle);
            preXY[i * 2 + 1] = (float) Math.sin(angle);
        }
    }

    public void eatApple()
    {
        startEatTime.add(System.nanoTime());
    }

    // command:
    //       1
    //      ____
    //     |    |
    //  2  |____|  0
    //
    //       3
    public void move(int curX, int curY, int lastCommand, int command, int length)
    {
        GLPoint2D point = new GLPoint2D(curX + 0.5f, curY + 0.5f);

        // Выполняется редко, поэтому в оптимизации не нуждается
        if (lastCommand == 0)
        {
            if      (command == 1) trajectory.push(point, GLTrajectoryCell.L2U);
            else if (command == 3) trajectory.push(point, GLTrajectoryCell.L2D);
            else                   trajectory.push(point, GLTrajectoryCell.L2R);
        }
        else if (lastCommand == 1)
        {
            if      (command == 2) trajectory.push(point, GLTrajectoryCell.D2L);
            else if (command == 0) trajectory.push(point, GLTrajectoryCell.D2R);
            else                   trajectory.push(point, GLTrajectoryCell.D2U);
        }
        else if (lastCommand == 2)
        {
            if      (command == 3) trajectory.push(point, GLTrajectoryCell.R2D);
            else if (command == 1) trajectory.push(point, GLTrajectoryCell.R2U);
            else                   trajectory.push(point, GLTrajectoryCell.R2L);
        }
        else if (lastCommand == 3)
        {
            if      (command == 0) trajectory.push(point, GLTrajectoryCell.U2R);
            else if (command == 2) trajectory.push(point, GLTrajectoryCell.U2L);
            else                   trajectory.push(point, GLTrajectoryCell.U2D);
        }

        if (trajectory.size() - 1 > length)
            trajectory.pop();

        lastLength = currentLength;
        currentLength = length;
    }

    private float getRadius(float pos, float length)
    {
        float startTail = length * 3 / 5;
        float appleEnd = startTail;

        float res = mainRadius;

        int i = 0;
        while (i < startEatTime.size())
        {
            float applePos = (System.nanoTime() - startEatTime.get(i)) / 1e9f;
            if (applePos > appleEnd)
            {
                startEatTime.remove(i);
                continue;
            }

            float appleRadius = APPLE_RADIUS * (1 - applePos / appleEnd);
            appleRadius += mainRadius; // Толщина стенок кишечника
            float d = (pos + appleRadius - applePos) * 0.7f; // Как растягивать ябколо вдоль змейки

            if (-appleRadius < d && d < appleRadius)
            {
                float r = (float) Math.sqrt(appleRadius * appleRadius - d * d);
                if (r > res) res = r;
            }

            i++;
        }

        // Хвост
        if (pos > startTail)
            res *= (float) Math.sqrt(((length - pos) / (float)(length - startTail)));

        return res;
    }

    private void buildHead(float x, float y, float z, float dx, float dz, float radius)
    {
        float ln = mainRadius / ((float) Math.sqrt(dx * dx + dz * dz));
        radius = radius / mainRadius;

        matrix.put(IDENTITY_MATRIX);
        matrix.position(0);

        matrix.put(0 * 4 + 0, -dz * ln);
        matrix.put(1 * 4 + 0, 0);
        matrix.put(2 * 4 + 0, dx * ln);

        matrix.put(0 * 4 + 1, 0);
        matrix.put(1 * 4 + 1, mainRadius);
        matrix.put(2 * 4 + 1, 0);

        matrix.put(0 * 4 + 2, dx * ln);
        matrix.put(1 * 4 + 2, 0);
        matrix.put(2 * 4 + 2, dz * ln);

        glPushMatrix();
        glTranslatef(x, y, z);
        glMultMatrixf(matrix);

        glBegin(GL_TRIANGLES);

        int count = Math.round(pointPerCell * 0.6f);

        float[] currentPoints = evenPoints;

        for (int i = 0; i < count; i++)
        {
            float realI = i / (float) (count - 1);
            float cRadius = radius * ((float) Math.sqrt(realI) * 0.5f + 1);

            currentPoints = (i % 2 == 0) ? evenPoints : oddPoints;

            float preZ = i / pointPerCell / mainRadius;

            for (int j = 0; j < cycleCount; j++)
            {
                if (j < cycleCount / 2)
                {
                    int preId = (j * 2 + (i % 2)) * 2;

                    currentPoints[j * 3 + 0] = preXY[preId + 0] * cRadius;
                    currentPoints[j * 3 + 1] = preXY[preId + 1] * cRadius;
                    currentPoints[j * 3 + 2] = preZ;
                }
                else
                {
                    currentPoints[j * 3 + 0] = (4 * j - 2 * cycleCount) / (float) (cycleCount - 2 * (i % 2)) * cRadius - cRadius;
                    currentPoints[j * 3 + 1] = 0;
                    currentPoints[j * 3 + 2] = preZ;
                }

                float k = (float)Math.sqrt(1 - realI) * 0.7f + 0.3f;
                currentPoints[j * 3 + 0] *= k;
                currentPoints[j * 3 + 1] *= (1 - realI) * 0.7f + 0.3f;
                currentPoints[j * 3 + 1] += realI * 0.4f * (radius - 1) + realI * 0.4f;
            }

            if (i == 0) continue;

            // Строим оболочку для evenPoints и oddPoints
            for (int j1 = 0; j1 < cycleCount; j1++)
            {
                int j2 = j1 + 1;
                if (j2 >= cycleCount) j2 = 0;

                // Построение треугольника even[j1], even[j2], odd[j1]
                gluMyNormal(evenPoints, evenPoints, oddPoints, j1 * 3, j2 * 3, j1 * 3, i % 2 == 0);
                glVertex3f(evenPoints[j1 * 3 + 0], evenPoints[j1 * 3 + 1], evenPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);

                // Построение треугольника even[j1], even[j2], odd[j1]
                gluMyNormal(evenPoints, evenPoints, oddPoints, j1 * 3, j2 * 3, j1 * 3, i % 2 == 1);
                glVertex3f(evenPoints[j1 * 3 + 0], -evenPoints[j1 * 3 + 1], evenPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], -evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], -oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);

                // Построение треугольника odd[j1], odd[j2], even[j2]
                gluMyNormal(oddPoints, oddPoints, evenPoints, j2 * 3, j1 * 3, j2 * 3, i % 2 == 0);
                glVertex3f(oddPoints[j2 * 3 + 0], oddPoints[j2 * 3 + 1], oddPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);

                // Построение треугольника odd[j1], odd[j2], even[j2]
                gluMyNormal(oddPoints, oddPoints, evenPoints, j2 * 3, j1 * 3, j2 * 3, i % 2 == 1);
                glVertex3f(oddPoints[j2 * 3 + 0], -oddPoints[j2 * 3 + 1], oddPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], -oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], -evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
            }
        }

        glEnd();

        glNormal3f(0, 0, 1);

        glBegin(GL_POLYGON);
        for (int i = 0; i < cycleCount; i++)
            glVertex3f(currentPoints[i * 3 + 0], currentPoints[i * 3 + 1], currentPoints[i * 3 + 2]);
        glEnd();

        glBegin(GL_POLYGON);
        for (int i = 0; i < cycleCount; i++)
            glVertex3f(currentPoints[i * 3 + 0], -currentPoints[i * 3 + 1], currentPoints[i * 3 + 2]);
        glEnd();

        glPopMatrix();
    }

    public void build(float a)
    {
        float offset = 1 - a;
        float length = lastLength + (currentLength - lastLength) * a;

        // Получение скелета
        int count = trajectory.calculate(buffer, pointPerCell, offset, length);

        if (count <= 1) return;

        float curRadius = getRadius(0, length);

        glColor3fv(color);

        buildHead(buffer[2 * 0 + 0], curRadius, buffer[2 * 0 + 1],
                buffer[2 * 0 + 0] - buffer[2 * 1 + 0], buffer[2 * 0 + 1] - buffer[2 * 1 + 1], curRadius);

        // Построение обертки скелета
        float[] currentPoints = evenPoints;

        glBegin(GL_TRIANGLES);

        for (int i = 0; i < count - 1; i++)
        {
            // Меняем местами evenPoints и oddPoints
            currentPoints = (i % 2 == 0) ? evenPoints : oddPoints;

            // Рассчет текущего радиуса
            curRadius = getRadius(i / pointPerCell, length);

            float dx = buffer[2 * (i + 1) + 1] - buffer[2 * i + 1];
            float dz = buffer[2 * i + 0] - buffer[2 * (i + 1) + 0];
            assert dx * dx + dz * dz > 1e-9f;
            float ln = ((float) Math.sqrt(dx * dx + dz * dz));
            dx = dx / ln * curRadius;
            dz = dz / ln * curRadius;

            // Насчитываем currentPoint
            for (int j = 0; j < cycleCount; j++)
            {
                int preId = (j * 2 + (i % 2)) * 2;

                currentPoints[j * 3 + 0] = preXY[preId + 0] * dx + buffer[2 * i + 0];
                currentPoints[j * 3 + 1] = preXY[preId + 1] * curRadius + curRadius;
                currentPoints[j * 3 + 2] = preXY[preId + 0] * dz + buffer[2 * i + 1];
            }

            if (i == 0) continue;

            // Строим оболочку для evenPoints и oddPoints
            for (int j1 = 0; j1 < cycleCount; j1++)
            {
                int j2 = j1 + 1;
                if (j2 >= cycleCount) j2 = 0;

                // Построение треугольника even[j1], even[j2], odd[j1]
                gluMyNormal(evenPoints, evenPoints, oddPoints, j1 * 3, j2 * 3, j1 * 3, i % 2 == 0);
                glVertex3f(evenPoints[j1 * 3 + 0], evenPoints[j1 * 3 + 1], evenPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);

                // Построение треугольника odd[j1], odd[j2], even[j2]
                gluMyNormal(oddPoints, oddPoints, evenPoints, j2 * 3, j1 * 3, j2 * 3, i % 2 == 0);
                glVertex3f(oddPoints[j2 * 3 + 0], oddPoints[j2 * 3 + 1], oddPoints[j2 * 3 + 2]);
                glVertex3f(oddPoints[j1 * 3 + 0], oddPoints[j1 * 3 + 1], oddPoints[j1 * 3 + 2]);
                glVertex3f(evenPoints[j2 * 3 + 0], evenPoints[j2 * 3 + 1], evenPoints[j2 * 3 + 2]);
            }
        }

        for (int j1 = 0; j1 < cycleCount; j1++)
        {
            int j2 = j1 + 1;
            if (j2 >= cycleCount) j2 = 0;

            float[] end = {buffer[(count - 1) * 2 + 0],
                    getRadius((count - 1) / pointPerCell, length),
                    buffer[(count - 1) * 2 + 1]};

            // построение треугольника currentPoints[j1], currentPoints[j2], end
            gluMyNormal(currentPoints, currentPoints, end, j1 * 3, j2 * 3, 0, false);
            glVertex3f(currentPoints[j1 * 3 + 0], currentPoints[j1 * 3 + 1], currentPoints[j1 * 3 + 2]);
            glVertex3f(currentPoints[j2 * 3 + 0], currentPoints[j2 * 3 + 1], currentPoints[j2 * 3 + 2]);
            glVertex3f(end[0], end[1], end[2]);
        }

        glEnd();
    }

    public void setPointPerCell(float pointPerCell)
    {
        assert pointPerCell < 30 : "Нужно будет менять принцип работы";
        this.pointPerCell = pointPerCell;
    }

    public void setColor(float r, float g, float b)
    {
        color[0] = r;
        color[1] = g;
        color[2] = b;
    }
}
