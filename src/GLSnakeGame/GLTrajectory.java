package GLSnakeGame;

import java.util.ArrayDeque;
import java.util.Iterator;

// Work like queue
public class GLTrajectory
{
    private float sinOffset = 0; // offset sin в конце траектории
    private ArrayDeque<GLPoint2D> points = new ArrayDeque<GLPoint2D>();
    private ArrayDeque<GLTrajectoryCell> trajectory = new ArrayDeque<GLTrajectoryCell>();

    public void push(GLPoint2D point, GLTrajectoryCell trajectoryCell)
    {
        sinOffset += trajectoryCell.getLength();
        if (sinOffset > Math.PI * 2) sinOffset -= Math.PI * 2;
        assert 0 <= sinOffset && sinOffset <= Math.PI * 2 : "Придется делать нормальную нормализацию";

        points.addLast(point);
        trajectory.addLast(trajectoryCell);
    }

    public void pop()
    {
        points.removeFirst();
        trajectory.removeFirst();
    }

    public int size()
    {
        return trajectory.size();
    }

    // Генерирует массив точек в формате:
    //      x = out[n * 2 + 0]
    //      y = out[n * 2 + 1]
    //  n = 0 - голова,   n = return - 1 - хвост
    //
    // Генерируюется ровно length последних точек, отступая от начала offset (<= 1).
    // Для каждой клетки будет сгенерировано pointsPerCell точек
    public int calculate(float[] out, float pointsPerCell, float offset, float length)
    {
//        System.out.println("!!! " + offset + " " + length);
        assert 0 <= offset && offset <= 1 : "Offset have to be from range [0; 1]";
        assert 0 <= length && length + offset <= trajectory.size() : "Incorrect length";
        assert 2 <= pointsPerCell : "Too few points";

        int amountOfPoint = (int) Math.ceil(pointsPerCell * length - pointsPerCell / 10) + 1;
        assert out.length >= amountOfPoint : "Too little out array";

        int count = 0;
        Iterator trajectory_it = trajectory.descendingIterator();
        Iterator points_it = points.descendingIterator();

        GLPoint2D point = (GLPoint2D) points_it.next();
        GLTrajectoryCell cell = (GLTrajectoryCell) trajectory_it.next();

        float shift = sinOffset;
        GLPoint2D res = new GLPoint2D(0, 0);

        for (int i = 0; i < amountOfPoint; i++)
        {
            float pos;

            if (i != amountOfPoint - 1)
                pos = i / pointsPerCell + offset - count;
            else
                pos = offset + length - count;

            if (pos > 1)
            {
                // С этой ячейкой все, идем в следующую
                shift -= cell.getLength();
                if (shift < 0) shift += Math.PI * 2;

                count++; pos--;
                point = (GLPoint2D) points_it.next();
                cell = (GLTrajectoryCell) trajectory_it.next();
            }

            float d = pos * cell.getLength();
            cell.calcPoint(res, cell.getLength() - d, (float) Math.sin((shift - d) * 5) * 0.0625f);
            out[i * 2 + 0] = res.x + point.x;
            out[i * 2 + 1] = res.y + point.y;
        }

        return amountOfPoint;
    }
}