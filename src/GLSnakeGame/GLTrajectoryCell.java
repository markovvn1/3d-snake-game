package GLSnakeGame;

public enum GLTrajectoryCell
{
    // L - Left, U - Up, R - Right, D - down
    R2L(0), L2R(1),
    D2U(2), U2D(3),

    D2L(4), L2D(5),
    R2D(6), D2R(7),
    U2R(8), R2U(9),
    L2U(10), U2L(11);

    final private int id;

    GLTrajectoryCell(int id)
    {
        this.id = id;
    }

    public float getLength()
    {
        if (id < 4) return 1;
        return (float)Math.PI / 4;
    }

    // Рассчитывает координаты точки на растоянии d от начала траектории, смещенной
    // на shift по направлению нормали к этой точке.
    // Координаты относительно центра траектории
    public void calcPoint(GLPoint2D out, float d, float shift)
    {
        assert 0 <= d && d <= getLength();


        if ((id % 2) == 1)
        {
            d = getLength() - d;
            shift = -shift;
        }


        if (id < 4)
        {
            int type = id >> 1;

            if (type == 0) {out.x = -d + 0.5f; out.y = shift; return;}
                           {out.x = shift; out.y =  d - 0.5f; return;}
        }
        else
        {
            int type = (id - 4) >> 1;

            float sin = (float) Math.sin(2 * d);
            float cos = (float) Math.cos(2 * d);

            float x = cos / 2 - 0.5f + shift * cos;
            float y = sin / 2 - 0.5f + shift * sin;

            if (type == 0) {out.x =  x; out.y =  y; return;}
            if (type == 1) {out.x = -y; out.y =  x; return;}
            if (type == 2) {out.x = -x; out.y = -y; return;}
                           {out.x =  y; out.y = -x; return;}
        }
    }

    public static void runTests()
    {
        GLPoint2D data = new GLPoint2D(0, 0);

        GLTrajectoryCell.R2L.calcPoint(data, 0.2f, 0);
        System.out.println("Вывод:     " + GLTrajectoryCell.R2L.getLength() + " " + data);
        System.out.println("Правильно: 1.0 <0.3, 0.0>");
        GLTrajectoryCell.L2R.calcPoint(data, 0.2f, 0);
        System.out.println("Вывод:     " + GLTrajectoryCell.L2R.getLength() + " " + data);
        System.out.println("Правильно: 1.0 <-0.3, 0.0>");
        System.out.println();

        GLTrajectoryCell.R2U.calcPoint(data, 0.2f, 0);
        System.out.println("Вывод:     " + GLTrajectoryCell.R2U.getLength() + " " + data);
        System.out.println("Правильно: 0.7853982 <0.30529088, 0.03946948>");
        GLTrajectoryCell.R2D.calcPoint(data, 0.2f, 0);
        System.out.println("Вывод:     " + GLTrajectoryCell.R2D.getLength() + " " + data);
        System.out.println("Правильно: 0.7853982 <0.30529082, -0.03946951>");
        System.out.println();

        GLTrajectoryCell.U2D.calcPoint(data, 0.2f, 0.4f);
        System.out.println("Вывод:     " + data);
        System.out.println("Правильно: <-0.4, 0.3>");
        GLTrajectoryCell.D2U.calcPoint(data, 0.2f, 0.4f);
        System.out.println("Вывод:     " + data);
        System.out.println("Правильно: <0.4, -0.3>");
        System.out.println();

        GLTrajectoryCell.R2U.calcPoint(data, 0.2f, 0.4f);
        System.out.println("Вывод:     " + data);
        System.out.println("Правильно: <0.4610582, 0.4078939>");
        GLTrajectoryCell.R2U.calcPoint(data, 0.2f, -0.4f);
        System.out.println("Вывод:     " + data);
        System.out.println("Правильно: <0.14952357, -0.32895494>");
    }
}