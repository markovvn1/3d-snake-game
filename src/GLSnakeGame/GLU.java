package GLSnakeGame;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL33.*;

public class GLU
{
    public static final float[] IDENTITY_MATRIX =
            new float[] {
                    1.0f, 0.0f, 0.0f, 0.0f,
                    0.0f, 1.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, 1.0f, 0.0f,
                    0.0f, 0.0f, 0.0f, 1.0f };

    private static final FloatBuffer matrix = BufferUtils.createFloatBuffer(16);
    private static final float[] forward = new float[3];
    private static final float[] side = new float[3];
    private static final float[] up = new float[3];


    private static void setIdentity()
    {
        int oldPos = matrix.position();
        matrix.put(IDENTITY_MATRIX);
        matrix.position(oldPos);
    }

    private static void normalize(float[] v) {
        float r;

        r = (float)Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        if (r == 0.0) return;

        r = 1.0f / r;

        v[0] *= r;
        v[1] *= r;
        v[2] *= r;
    }

    private static void cross(float[] result, float[] v1, float[] v2) {
        result[0] = v1[1] * v2[2] - v1[2] * v2[1];
        result[1] = v1[2] * v2[0] - v1[0] * v2[2];
        result[2] = v1[0] * v2[1] - v1[1] * v2[0];
    }

    public static void gluPerspective(float fovy, float aspect, float zNear, float zFar) {
        float radians = fovy / 2 * (float)Math.PI / 180;

        float deltaZ = zFar - zNear;
        float sine = (float)Math.sin(radians);

        if ((deltaZ == 0) || (sine == 0) || (aspect == 0)) return;

        float cotangent = (float)Math.cos(radians) / sine;

        setIdentity();

        matrix.put(0 * 4 + 0, cotangent / aspect);
        matrix.put(1 * 4 + 1, cotangent);
        matrix.put(2 * 4 + 2, - (zFar + zNear) / deltaZ);
        matrix.put(2 * 4 + 3, -1);
        matrix.put(3 * 4 + 2, -2 * zNear * zFar / deltaZ);
        matrix.put(3 * 4 + 3, 0);

        glMultMatrixf(matrix);
    }

    public static void gluLookAt(
        float eyex, float eyey, float eyez,
        float centerx, float centery, float centerz,
        float upx, float upy, float upz)
    {
        forward[0] = centerx - eyex;
        forward[1] = centery - eyey;
        forward[2] = centerz - eyez;

        up[0] = upx;
        up[1] = upy;
        up[2] = upz;

        normalize(forward);

        /* Side = forward x up */
        cross(side, forward, up);
        normalize(side);

        /* Recompute up as: up = side x forward */
        cross(up, side, forward);

        setIdentity();
        matrix.put(0 * 4 + 0, side[0]);
        matrix.put(1 * 4 + 0, side[1]);
        matrix.put(2 * 4 + 0, side[2]);

        matrix.put(0 * 4 + 1, up[0]);
        matrix.put(1 * 4 + 1, up[1]);
        matrix.put(2 * 4 + 1, up[2]);

        matrix.put(0 * 4 + 2, -forward[0]);
        matrix.put(1 * 4 + 2, -forward[1]);
        matrix.put(2 * 4 + 2, -forward[2]);

        glMultMatrixf(matrix);
        glTranslatef(-eyex, -eyey, -eyez);
    }


    // cross-product
    public static void gluMyNormal(float[] p1, float[] p2, float[] p3, int i1, int i2, int i3, boolean inverse)
    {
        float x1 = p1[i1 + 0] - p2[i2 + 0];
        float y1 = p1[i1 + 1] - p2[i2 + 1];
        float z1 = p1[i1 + 2] - p2[i2 + 2];
        float x2 = p1[i1 + 0] - p3[i3 + 0];
        float y2 = p1[i1 + 1] - p3[i3 + 1];
        float z2 = p1[i1 + 2] - p3[i3 + 2];

        if (!inverse)
            glNormal3f(y1 * z2 - y2 * z1,
                    x2 * z1 - x1 * z2,
                    x1 * y2 - x2 * y1);
        else
            glNormal3f(y2 * z1 - y1 * z2,
                    x1 * z2 - x2 * z1,
                    x2 * y1 - x1 * y2);
    }

}
