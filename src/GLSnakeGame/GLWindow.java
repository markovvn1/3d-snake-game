package GLSnakeGame;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWKeyCallbackI;
import org.lwjgl.opengl.GL;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/*
    Модуль работы с окнами.

    Важно: окно создается скрытым. Когда все готово, вызовите .show() чтобы показать окно
 */

public class GLWindow implements GLFWKeyCallbackI
{
    // action
    public static final int GL_KEY_RELEASE = 0;
    public static final int GL_KEY_PRESS = 1;

    private long id;
    private IntBuffer width = BufferUtils.createIntBuffer(1);
    private IntBuffer height = BufferUtils.createIntBuffer(1);

    private GLKeyCallbackI keyCallback = null;

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods)
    {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
            glfwSetWindowShouldClose(window, true);

        if (keyCallback == null) return;

        int groupNum = -1;
        int command = -1;

        if (key == GLFW_KEY_UNKNOWN)
        {
            switch (scancode)
            {
                case 114:
                    command = 0; groupNum = 0; break;
                case 111:
                    command = 1; groupNum = 0; break;
                case 113:
                    command = 2; groupNum = 0; break;
                case 116:
                    command = 3; groupNum = 0; break;
            }
        }
        else switch (key)
        {
            case GLFW_KEY_RIGHT:
                command = 0; groupNum = 0; break;
            case GLFW_KEY_UP:
                command = 1; groupNum = 0; break;
            case GLFW_KEY_LEFT:
                command = 2; groupNum = 0; break;
            case GLFW_KEY_DOWN:
                command = 3; groupNum = 0; break;

            case GLFW_KEY_D:
                command = 0; groupNum = 1; break;
            case GLFW_KEY_W:
                command = 1; groupNum = 1; break;
            case GLFW_KEY_A:
                command = 2; groupNum = 1; break;
            case GLFW_KEY_S:
                command = 3; groupNum = 1; break;

            default:
                break;
        }

        if (command == -1) return;

        if (action == GLFW_PRESS) keyCallback.keyEvent(GL_KEY_PRESS, groupNum, GLKeyCommand.values()[command]);
        if (action == GLFW_RELEASE) keyCallback.keyEvent(GL_KEY_RELEASE, groupNum, GLKeyCommand.values()[command]);
    }

    public void init(int width, int height, String title)
    {
        if (!glfwInit()) throw new IllegalStateException("Unable to initialize GLFW");

        // Create window
        glfwWindowHint(GLFW_SAMPLES, 4);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        id = glfwCreateWindow(width, height, title, NULL, NULL);

        // Set callback
        glfwSetKeyCallback(id, this);

        // Make the OpenGL context current
        glfwMakeContextCurrent(id);

        // Enable v-sync
        glfwSwapInterval(1);

        // Prepare context for OpenGL
        GL.createCapabilities();


        update();
    }

    public void deinit()
    {
        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(id);
        glfwDestroyWindow(id);

        // Terminate GLFW
        glfwTerminate();
    }

    public void show()
    {
        glfwShowWindow(id);
        update();
    }

    public void hide()
    {
        glfwHideWindow(id);
    }

    public void swapBuffers()
    {
        glfwSwapBuffers(id);
    }

    public void update()
    {
        glfwPollEvents();
    }

    public boolean shouldClose()
    {
        return glfwWindowShouldClose(id);
    }

    public int getWidth()
    {
        glfwGetWindowSize(id, width, height);
        return width.get(0);
    }

    public int getHeight()
    {
        glfwGetWindowSize(id, width, height);
        return height.get(0);
    }

    public void setKeyCallback(GLKeyCallbackI keyCallback)
    {
        this.keyCallback = keyCallback;
    }
}
