package ru.tom.vova;

import GLSnakeGame.*;
import snakeGame.*;

import java.util.ArrayList;

/*
    Краткое описание проекта.
    Проект условно поделен на 2 части:
        - механника игры (движение змеек, расчет столкновений)
        - графика (как игра выглядит)

    К графике относиться только то, что начинается с GL, к механнике игры же все, что не начинается с GL.
    Класс Main служит посредником между механникой и графикой. Если нужно изменить механнику игры, то
    достаточно просто изменить несколько строк в этом классе - чтобы мутоды из GLGame вызывались с такими
    же параметрами в такое же время.

    Описание графической части находится в GLGame. Рекомендую его прочитать, так как там написана очень
    важная особенность графической составляющей
 */

/*
    Short description of project.
    Project are separated into 2 part:
        - game (how snakes should move, check collision, etc)
        - graphic (how snakes look like, how apple look like, etc)

    These two systems are fully independent.
    To make connection between them you should use this class. You should send to GLGame correct massage
    in correct time. You can bring this message from class Game. So, everything that this class does is
    redirect message from game part to graphic part.
 */


// TODO: Окно показывается только после окончания загрузки
// TODO: Проработать все виды столкновений
// TODO: Добавить тени
// TODO: SnakeBrainSmart алгорим избегания столкновений с головой. Нормальные размеры поля
// TODO: Сделать счетчик очков
// TODO: Проработать все виды assert'ов в GLGame (проверка для nextFrame)

public class Main implements GLKeyCallbackI
{
    static final long TIME_PER_CELL = (long)0.7e9; // Время в наносекундах для прохождения змейкой одной клетки

    Game game = new Game();
    GLGame glGame = new GLGame();

    public void init()
    {
        System.out.println(" ____              _         ____                      ");
        System.out.println("/ ___| _ __   __ _| | _____ / ___| __ _ _ __ ___   ___ ");
        System.out.println("\\___ \\| '_ \\ / _` | |/ / _ \\ |  _ / _` | '_ ` _ \\ / _ \\");
        System.out.println(" ___) | | | | (_| |   <  __/ |_| | (_| | | | | | |  __/");
        System.out.println("|____/|_| |_|\\__,_|_|\\_\\___|\\____|\\__,_|_| |_| |_|\\___|");
        System.out.println();
        System.out.println("                                              by Markovvn1");

        System.out.println("Loading... (it may takes some seconds)");

        game.init();
        // Сообщить графическому движку, что инициализация начинается
        glGame.init(game.getMazeWidth(), game.getMazeHeight(), game.snakes.size(), true);
        glGame.getWindow().setKeyCallback(this); // Регистрация функции для обработки нажатий клавиш

        System.out.println();
        System.out.println("Full name: Markov Vladimir");
        System.out.println("Group: B19-03");
        System.out.println("Type of work: optional task (for competition)");
        System.out.println();
        System.out.println("How to play:");
        System.out.println("Your snake is blue. Use arrows on the keyboard to control it. The red snake has its own smart brain.");
        System.out.println("Score: one apple = one score, death - minus 5 scores");

        // Установка позиции яблока
        glGame.setApplePos(game.getApple().x, game.getApple().y);

        // Инициализация графической оболочки змеек (загрузка данных о змейках)
        for (Snake snake : game.snakes)
        {
            ArrayList<Position> body = snake.getBody();

            for (int i = 0; i < body.size(); i++)
                glGame.updateSnake(snake.getId(), body.get(i).x, body.get(i).y, i + 1);
        }

        System.out.println();
        System.out.println("Are you ready? [press Enter]");
        System.out.flush();
        try
        {
            System.in.read();
        }
        catch (Exception e)
        {
            glGame.deinit();
            return;
        }
    }

    public void main()
    {
        init();

        // Сообщить графическому движку, что игра начинается
        glGame.start();

        long startTime = System.nanoTime();
        int frame = -1;

        while (!glGame.getWindow().shouldClose())
        {
            long cTime = System.nanoTime() - startTime;
            int cFrame = (int)(cTime / TIME_PER_CELL);

            glGame.getWindow().update(); // To catch event again (optional)

            while (cFrame > frame && game.isActive())
            {
                if (!game.mainLoop())
                {
                    // Сообщить графическому движку, что игра подходит к концу
                    glGame.end();

                    // Сообщить результат игры (очки)
                    glGame.setScore(
                            Math.max(0, game.snakes.get(0).length() - (game.snakes.get(0).isAlive() ? 2 : 7)),
                            Math.max(0, game.snakes.get(1).length() - (game.snakes.get(1).isAlive() ? 2 : 7)));
                }

                // Сообщить графическому движку, что поступает информация о следующем кадре
                glGame.nextFrame();

                // Отправка данных графическому движку
                for (Snake snake : game.snakes)
                {
                    // Так как движок это самостоятельная система, типы не должны пересекаться, а значит
                    // нужно их преобразовывать ручками
                    GLTypeOfCollision type = GLTypeOfCollision.NO_COLLISION;
                    switch (snake.getTypeOfCollision())
                    {
                        case TO_HEAD: type = GLTypeOfCollision.TO_HEAD; break;
                        case TO_BODY: type = GLTypeOfCollision.TO_BODY; break;
                        case TO_WALL: type = GLTypeOfCollision.TO_WALL; break;
                    }

                    glGame.updateSnake(snake.getId(), snake.getHead().x, snake.getHead().y, snake.length(), type);
                }

                // Если позиция яблока изменилась
                if (game.getAppleEatenBy() != -1)
                {
                    // Who have eaten my apple?
                    glGame.appleEatenBy(game.getAppleEatenBy());
                    glGame.setApplePos(game.getApple().x, game.getApple().y);
                }

                frame++;
            }

            // Показать картинку. Если движок больше не планирует ничего рисовать, то все, конец
            if (!glGame.render(cFrame, (cTime % TIME_PER_CELL) / (float)TIME_PER_CELL))
            {
                // Или нет :)
                // break;
            }
        }

        glGame.deinit();
    }

    public static void main(String[] args)
    {
        (new Main()).main();
    }

    @Override
    public void keyEvent(int action, int groupNum, GLKeyCommand command)
    {
        if (action != GLWindow.GL_KEY_PRESS) return;

        if (groupNum == 0)
        {
            switch (command)
            {
                case RIGHT:
                    game.snakes.get(1).getBrain().setCommand(SnakeCommand.LEFT);
                    break;
                case UP:
                    game.snakes.get(1).getBrain().setCommand(SnakeCommand.UP);
                    break;
                case LEFT:
                    game.snakes.get(1).getBrain().setCommand(SnakeCommand.RIGHT);
                    break;
                case DOWN:
                    game.snakes.get(1).getBrain().setCommand(SnakeCommand.DOWN);
                    break;
            }
        }
    }
}