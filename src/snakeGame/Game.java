package snakeGame;

import java.util.ArrayList;
import java.util.Random;

/*
    Так как змейка потеряла возможность поворачиваться на месте, ей нужно выбирать путь на будущее.
    То есть, если она хочет повернуть направо, ей нужно об этом задуматься заранее на 1 ход.

    По этой причине я применяю некоторую хитрость. Раньше змейча принимала решение, а потом шагала. Теперь же
    она шагает так как она на прошлом ходу решила, а потом принимает решение куда она пойдет на следующем шаге
 */

public class Game
{
    static final int MAZE_SIZE = 8;

    // maze[i][j]: -2 if apple,   -1 if is free,   n >= 0 if snake n here
    public int[][] maze = new int[MAZE_SIZE][MAZE_SIZE];
    public ArrayList<Snake> snakes = new ArrayList<Snake>();

    private Position apple = null;
    private boolean needToRespawnApple = true;
    private int appleEatenBy = -1;

    private boolean active = false;

    private Random rand = new Random();


    /*
        Initialization before game start: prepare maze, create snakes, put apple
     */
    public void init()
    {
        // Clean maze
        for (int y = 0; y < MAZE_SIZE; y++)
            for (int x = 0; x < MAZE_SIZE; x++)
                maze[y][x] = -1;

        // Initialize snakes
        snakes.add(new Snake(snakes.size(), new SnakeBrainSmart()));
        snakes.add(new Snake(snakes.size(), new SnakeBrainControl()));

        snakes.get(0).init(0, 2, this);
        snakes.get(1).init(7, 5, this);

        // Snake must start from size 2
        snakes.get(0).move(SnakeCommand.UP, true);
        snakes.get(0).updateLastSize(); // Предыдущие изменения размера не анимировать

        snakes.get(1).getBrain().setCommand(SnakeCommand.DOWN); // Значение по умолчанию (мы управляем этой змейкой)
        snakes.get(1).move(SnakeCommand.DOWN, true);
        snakes.get(1).updateLastSize(); // Предыдущие изменения размера не анимировать

        for (Snake snake : snakes)
            snake.update();

        updateApple();

        active = true;
    }

    /*
        Main loop of game. When you call this method, each snakes:
        1. ask its brain about next direction
        2. move (and check collisions)

        You get false if game has finished
     */
    public boolean mainLoop()
    {
        if (!active) return false;

        appleEatenBy = -1;

        // Apply move
        for (Snake snake : snakes)
            snake.nextMove();

        // Update snakes, check collisions
        for (Snake snake : snakes)
            snake.update();

        // Respawn apple, if it necessary
        updateApple();

        // If at least one snake dead then game end
        for (Snake snake : snakes)
            if (!snake.isAlive()) active = false;

        return active;
    }


    /*
        This method generate String, which describe maze
     */
    public String maze2str()
    {
        assert snakes.size() < 10 : "To match snakes";

        char[] str = new char[MAZE_SIZE * MAZE_SIZE * 2]; // Because JAVA

        for (int y = 0; y < MAZE_SIZE; y++)
            for (int x = 0; x < MAZE_SIZE; x++) {
                if (maze[y][x] >= 0)
                    str[(y * MAZE_SIZE + x) * 2 + 0] = (char)('0' + maze[y][x]);
                else
                if (maze[y][x] == -2)
                    str[(y * MAZE_SIZE + x) * 2 + 0] = 'a';
                else
                    str[(y * MAZE_SIZE + x) * 2 + 0] = '.';

                if (x != MAZE_SIZE - 1)
                    str[(y * MAZE_SIZE + x) * 2 + 1] = ' ';
                else
                    str[(y * MAZE_SIZE + x) * 2 + 1] = '\n';
            }

        // Draw heads of snakes
        for (Snake snake : snakes)
        {
            Position head = snake.getHead();
            str[(head.y * MAZE_SIZE + head.x) * 2 + 0] = 'X';
        }

        return new String(str);
    }

    /*
        Mark apple for respawn
     */
    void eatApple(Snake snake)
    {
        needToRespawnApple = true;
        appleEatenBy = snake.getId();

        maze[apple.y][apple.x] = -1;
    }

    /*
        If apple has been marked then respawn it
     */
    boolean updateApple()
    {
        if (!needToRespawnApple) return false;
        needToRespawnApple = false;

        for (int i = 0; i < 10000; i++)
        {
            apple = new Position(rand.nextInt(MAZE_SIZE), rand.nextInt(MAZE_SIZE));

            if (maze[apple.y][apple.x] >= 0) continue;
            maze[apple.y][apple.x] = -2;
            return true;
        }

        return false;
    }

    public Position getApple()
    {
        return apple;
    }

    public int getAppleEatenBy()
    {
        return appleEatenBy;
    }

    public boolean isActive()
    {
        return active;
    }

    public int getMazeWidth()
    {
        return MAZE_SIZE;
    }

    public int getMazeHeight()
    {
        return MAZE_SIZE;
    }

    @Override
    public String toString()
    {
        return maze2str();
    }
}
