package snakeGame;

public class Position {
    public int y, x;

    public Position(int y, int x) {
        this.y = y;
        this.x = x;
    }

    public boolean isValid(int height, int width) {
        return (y >= 0) && (x >= 0) && (y < height) && (x < width);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!obj.getClass().equals(getClass())) return false;

        return (((Position)obj).x == x) && (((Position)obj).y == y);
    }
}
