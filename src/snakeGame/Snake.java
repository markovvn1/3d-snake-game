package snakeGame;

import java.util.ArrayList;

/*
    Class Snake. Every snake has brain - class, which take decision about next move.
    Different snakes can has different brains. Now there exist 3 type of brain:
    SnakeBrainCrazy, SnakeBrainClever, SnakeBrainSmart
    Description for each type of brain you can find in relevant files
 */

public class Snake
{
    private boolean alive = true;
    private int id;
    private Game game;

    private SnakeBrain brain;
    private ArrayList<Position> body = new ArrayList<Position>();
    private int bodyLastSize = 0; // Длина змейки в предыдущем ходе
    private TypeOfCollision typeOfCollision = TypeOfCollision.NO_COLLISION;

    // for moving
    private boolean needUpdate = false;
    private SnakeCommand lastCommand = SnakeCommand.NONE;



    public Snake(int id, SnakeBrain brain)
    {
        this.id = id;
        this.brain = brain;
    }

    public void init(int y, int x, Game game)
    {
        this.game = game;

        body.clear();
        body.add(new Position(y, x));
        game.maze[y][x] = this.id;

        brain.init(id, game);
    }

    // Setting command for next move. After doing it for all snakes you should call .update()
    public boolean move(SnakeCommand command)
    {
        return move(command, false);
    }

    // If permanent_growing true then snake will grow (despite of no apple)
    // Setting command for next move. After doing it for all snakes you should call .update()
    public boolean move(SnakeCommand command, boolean permanent_growing)
    {
        assert needUpdate == false : "You should call .update() before call next .move()";

        lastCommand = command;
        updateLastSize();

        Position head = getHead();
        Position newHead = new Position(head.y + command.dy, head.x + command.dx);
        // Set new head, and then we will check whether it has collision or not
        body.add(newHead);

        if (!newHead.isValid(game.maze.length, game.maze[0].length))
        {
            kill(TypeOfCollision.TO_WALL);
            return false;
        }

        boolean growing = permanent_growing;
        if (!growing)
        {
            growing = game.maze[newHead.y][newHead.x] == -2;

            if (growing) game.eatApple(this);
        }

        if (!growing)
        {
            Position end = body.get(0); body.remove(0);

            game.maze[end.y][end.x] = -1;
        }

        needUpdate = true;

        return true;
    }

    public SnakeCommand nextMove()
    {
        SnakeCommand nextCommand = brain.nextMove();

        // Проверка на движение в обратную сторону
        if (nextCommand.inverse() == lastCommand)
            nextCommand = lastCommand;

        move(nextCommand);
        return nextCommand;
    }

    /*
        Update position of snake and check collisions
     */
    public void update()
    {
        if (!needUpdate) return;
        needUpdate = false;

        int headId = game.maze[getHead().y][getHead().x];

        if (headId >= 0)
        {
            // If it is head-to-head collision
            if (game.snakes.get(headId).getHead().equals(getHead()) && id != headId)
            {
                game.snakes.get(headId).kill(TypeOfCollision.TO_HEAD);
                kill(TypeOfCollision.TO_HEAD);
            }
            else
            {
                kill(TypeOfCollision.TO_BODY);
            }
            return;
        }

        game.maze[getHead().y][getHead().x] = id;
    }

    public void updateLastSize()
    {
        bodyLastSize = body.size();
    }

    public void kill(TypeOfCollision typeOfCollision)
    {
        alive = false;
        this.typeOfCollision = typeOfCollision;
    }

    public boolean isAlive()
    {
        return alive;
    }

    public int getId()
    {
        return id;
    }

    public SnakeBrain getBrain()
    {
        return brain;
    }

    public Position getHead()
    {
        return body.get(body.size() - 1);
    }

    // [0] - хвост, [size - 1] - голова
    public ArrayList<Position> getBody()
    {
        return body;
    }

    public TypeOfCollision getTypeOfCollision()
    {
        return typeOfCollision;
    }

    public int length()
    {
        return body.size();
    }
    public int lastLength()
    {
        return bodyLastSize;
    }
}
