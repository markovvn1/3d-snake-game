package snakeGame;

/*
    This is interface of brain for snake (for more information look at Snake.java)
 */

public abstract class SnakeBrain
{
    protected int id;
    protected Snake snake;
    protected Game game;

    public void init(int id, Game game)
    {
        this.id = id;
        this.snake = game.snakes.get(id);
        this.game = game;

        init();
    }

    public abstract void init();

    // Brain should choose next move
    public abstract SnakeCommand nextMove();

    // Give advice about next move (полезно когда нужно управлять змейкой, например, с клавиатуры)
    public abstract void setCommand(SnakeCommand command);
}
