package snakeGame;

import java.util.Random;

/*
    This is more clever brain, which try to protect snake from self-collision and from collision with other snakes.
    Choose next direction randomly
    Не застрахована от столкновения heat-to-head потому что змейки не могут предсказывать куда пойдут другие змейки
 */

public class SnakeBrainClever extends SnakeBrain
{
    private Random rand;

    @Override
    public void init()
    {
        rand = new Random();
    }

    @Override
    public SnakeCommand nextMove()
    {
        assert snake.getBody().size() >= 2 : "Snake should have at least 2 size";

        Position head = snake.getHead();

        // Generate array of correct move
        int count = 0;
        int[] correctCommand = new int[4];
        for (int i = 0; i < 4; i++)
        {
            SnakeCommand command = SnakeCommand.values()[i + 1];
            Position nextPos = new Position(head.y + command.dy, head.x + command.dx);

            // Do not touch board
            if (!nextPos.isValid(game.maze.length, game.maze[0].length)) continue;

            // Do not cross other snakes
            if (game.maze[nextPos.y][nextPos.x] >= 0) continue;

            correctCommand[count] = i;
            count++;
        }

        if (count == 0) return SnakeCommand.values()[rand.nextInt(4) + 1];

        // Randomly choose one command from correct
        return SnakeCommand.values()[correctCommand[rand.nextInt(count)] + 1];
    }

    @Override
    public void setCommand(SnakeCommand command)
    {
        // ignore
    }
}
