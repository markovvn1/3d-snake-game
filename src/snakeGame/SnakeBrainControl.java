package snakeGame;

public class SnakeBrainControl extends SnakeBrain
{
    private SnakeCommand nextCommand = SnakeCommand.NONE;

    @Override
    public void init()
    {

    }

    @Override
    public SnakeCommand nextMove()
    {
        return nextCommand;
    }

    @Override
    public void setCommand(SnakeCommand command)
    {
        nextCommand = command;
    }
}
