package snakeGame;

import java.util.Random;

/*
    This is type of smart brain. This brain try to protect from any type collision and try to
    choose the shortest path to apple. But this brain can make more complicated mistakes, like this (snake 0):

    . . . . . . . .      . . . . . . . .      . . . . . . . .      . . . . . . . .
    . . . . . . . .      . . . . . . . .      . . . . . . . .      . . . . . . . .
    . . . . 1 1 . .      . . . . 1 . . .      . . . . . . X .      . . . . . X 1 .
    . . . . 1 X . .      . . . . 1 1 X .      . . . . 1 1 1 .      . . . . . 1 1 .
    . a X 0 . . . .  ->  . X 0 0 . . . .  ->  . 0 0 0 . . . .  ->  . 0 0 0 . . . .
    . . . 0 . . . .      . . . 0 . . . .      . X . 0 . . . .      . 0 X 0 . . . .
    0 0 0 0 . . . .      0 0 0 0 . . . a      0 . 0 0 . . . a      . . 0 0 . . . a
    0 0 0 . . . . .      0 0 0 . . . . .      0 0 0 . . . . .      0 0 0 . . . . .
 */

public class SnakeBrainSmart extends SnakeBrain
{
    private Random rand;
    private int commandBack = -1;

    @Override
    public void init()
    {
        rand = new Random();
    }

    @Override
    public SnakeCommand nextMove()
    {
        assert snake.getBody().size() >= 2 : "Snake should have at least 2 size";

        Position head = snake.getHead();

        // Generate array of correct move
        int[] distanceToApple = new int[4];
        int punishment = game.maze.length * game.maze.length + game.maze[0].length * game.maze[0].length;

        for (int i = 0; i < 4; i++)
        {
            SnakeCommand command = SnakeCommand.values()[i + 1];
            Position nextPos = new Position(head.y + command.dy, head.x + command.dx);

            int dy = nextPos.y - game.getApple().y;
            int dx = nextPos.x - game.getApple().x;

            distanceToApple[i] = dy * dy + dx * dx;

            // Do not touch board
            if (!nextPos.isValid(game.maze.length, game.maze[0].length))
            {
                distanceToApple[i] += 8 * punishment;
                continue;
            }

            // Do not cross other snakes
            if (game.maze[nextPos.y][nextPos.x] >= 0)
            {
                distanceToApple[i] += 8 * punishment;
                continue;
            }

            // Стараемся, чтобы голова не была слишком близка к голове другой змейки
            for (int j = 0; j < game.snakes.size(); j++)
            {
                if (j == id) continue;
                Snake s = game.snakes.get(j);

                dy = s.getHead().y - nextPos.y;
                dx = s.getHead().x - nextPos.x;

                // Понизить приоритет выбора этой позиции
                int d = dy * dy + dx * dx;
                if (d <= 5)
                {
                    distanceToApple[i] += 4 * punishment;
                    distanceToApple[i] -= 4 * d;
                }
            }
        }

        // Choose the best one
        int minDistance = Integer.MAX_VALUE;
        int num = 0;

        for (int i = 0; i < 4; i++)
        {
            if (i == commandBack) continue;

            if (distanceToApple[i] < minDistance)
            {
                minDistance = distanceToApple[i];
                num = i;
            }
        }

        if (num == 0) commandBack = 1;
        if (num == 1) commandBack = 0;
        if (num == 2) commandBack = 3;
        if (num == 3) commandBack = 2;

        return SnakeCommand.values()[num + 1];
    }

    @Override
    public void setCommand(SnakeCommand command)
    {
        // ignore
    }
}
