package snakeGame;

public enum SnakeCommand {
    NONE(0, 0, "None"),
    UP(1, 0, "Up"),
    DOWN(-1, 0, "Down"),
    RIGHT(0, 1, "Right"),
    LEFT(0, -1, "Left");

    final public int dy, dx;
    final private String text;

    SnakeCommand(int dy, int dx, String text) {
        this.dy = dy;
        this.dx = dx;
        this.text = text;
    }

    public static SnakeCommand fromString(String s)
    {
        if (s.equalsIgnoreCase("N")) return UP;
        if (s.equalsIgnoreCase("S")) return DOWN;
        if (s.equalsIgnoreCase("E")) return RIGHT;
        if (s.equalsIgnoreCase("W")) return LEFT;
        return NONE;
    }

    public SnakeCommand inverse()
    {
        switch (this)
        {
            case RIGHT:
                return LEFT;
            case UP:
                return DOWN;
            case LEFT:
                return RIGHT;
            case DOWN:
                return UP;
            default:
                return NONE;
        }
    }

    @Override
    public String toString() {
        return this.text;
    }
}

