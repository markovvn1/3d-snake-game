package snakeGame;

public enum TypeOfCollision
{
    NO_COLLISION,
    TO_HEAD, // Collision between head of one snake and head of another snake
    TO_BODY, // Snake burn into body
    TO_WALL  // Snake burn into wall
}
